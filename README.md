# qubengage

### Components

- [/frontend](frontend/) - Frontend implementation
- [/backend/maxmin](backend/maxmin/) - Find Maximum and Minimum Attendance
- [/backend/risk](backend/risk/) - Risk of Student Failure
- [/backend/score](backend/score/) - Student Engagement Score
- [/backend/sort](backend/sort/) - Sort Attendance
- [/backend/sum](backend/sum/) - Total Attendance Hours
- [/backend/recognition](backend/recognition/) - Student Recognition
- [/backend/proxy](backend/proxy/) - Proxy Server

## Development

### Dependencies

- `docker`
- `docker-compose`

### Run locally

```sh
docker-compose up --build
```

### Build

Creating a new tag on Gitlab will trigger CI pipelines to build new docker images.


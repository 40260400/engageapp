# Deploy to Kubernetes

## Prerequisites

1. Install `kubectl` and `helm` tools
2. Add helm repo and update
    ```sh
    helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
    helm repo update
    ```

## Updating Nginx Ingress

```sh
helm upgrade ingress-nginx ingress-nginx/ingress-nginx -n ingress-nginx -f nginx-values.yml
```

## Updating Individual Components

```sh
kubectl apply -f frontend
kubectl apply -f backend/maxmin
kubectl apply -f backend/sort
kubectl apply -f backend/sum
kubectl apply -f backend/score
kubectl apply -f backend/risk
kubectl apply -f backend/recognition
kubectl apply -f backend/prox
```

## Creating new cluster

### Install Nginx Ingress

1. Create namespace
    ```sh
    kubectl create ns ingress-nginx
    ```
2. Install nginx with Helm:
    ```sh
    helm install ingress-nginx ingress-nginx/ingress-nginx -n ingress-nginx -f nginx-values.yml
    ```

### Install deployments

1. Create namespace
    ```sh
    kubectl create ns app
    ```
2. Configure access token on repository with Reporter `Role` and `read_registry` scope to container registry.
3. Create registry-key secret
    ```sh
    kubectl create secret docker-registry registry-key \
        --docker-server=registry.gitlab.com/<user>/<repo> \
        --docker-username=k8s --docker-password=<token> \
        -n app
    ```
4. Deploy all components:
    ```sh
    kubectl apply -f frontend
    kubectl apply -f backend/maxmin
    kubectl apply -f backend/sort
    kubectl apply -f backend/sum
    kubectl apply -f backend/score
    kubectl apply -f backend/risk
    kubectl apply -f backend/prox
    ```


# backend/proxy

Service Proxy

## Configuration


```yml
# /app/config.yml
services:
  - path: /maxmin
    addr: http://backend-maxmin:80
  - path: /risk
    addr: http://backend-risk:80
  - path: /score
    addr: http://backend-score:80
  - path: /sort
    addr: http://backend-sort:80
  - path: /sum
    addr: http://backend-sum:80
```

- `services` - List of services to proxy
    - `path` - Path prefix of the service
    - `addr` - Address pointing to the service


## Development

### Prerequisites

- `go`

### Install dependencies

```sh
go mod download
```

### Run tests

```sh
go test ./...
```

package main

import (
	"fmt"
	"log"
	"net/http"
	"qubengage/proxy/app"
	"qubengage/proxy/config"
	"time"
)

func main() {
	cfg, err := config.LoadFromFile("./config.yml")
	if err != nil {
		log.Fatal(err)
	}
	h, err := app.Load(cfg)
	if err != nil {
		log.Fatal(err)
	}
	srv := http.Server{
		Addr:         ":80",
		WriteTimeout: time.Second * 5,
		Handler:      h,
	}
	if err := srv.ListenAndServe(); err != nil {
		fmt.Printf("proxy failed: %s\n", err)
	}
}

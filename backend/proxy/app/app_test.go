package app

import (
	"bytes"
	"context"
	"io"
	"net/http"
	"net/http/httptest"
	"qubengage/proxy/config"
	"testing"

	"github.com/stretchr/testify/suite"
)

type AppTestSuite struct {
	suite.Suite
	srv    *httptest.Server
	foo    *httptest.Server
	bar    *httptest.Server
	ctx    context.Context
	cancel context.CancelFunc
}

func (s *AppTestSuite) BeforeTest(_, _ string) {
	s.ctx, s.cancel = context.WithCancel(context.Background())
	cfg := &config.Config{}
	s.foo = httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("foo"))
	}))
	s.bar = httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("bar"))
	}))
	cfg.Services = append(cfg.Services, &config.Service{
		Path: "/foo",
		Addr: "http://" + s.foo.Listener.Addr().String(),
	})
	cfg.Services = append(cfg.Services, &config.Service{
		Path: "/bar",
		Addr: "http://" + s.bar.Listener.Addr().String(),
	})
	h, err := Load(cfg)
	s.Require().NoError(err)
	s.srv = httptest.NewServer(h)
}

func (s *AppTestSuite) TearDownTest() {
	s.cancel()
}

func (s *AppTestSuite) AfterTest(_, _ string) {
	s.srv.Close()
	s.foo.Close()
	s.bar.Close()
}

func TestAppTestSuite(t *testing.T) {
	suite.Run(t, new(AppTestSuite))
}

func (s *AppTestSuite) TestFoo() {
	want := `foo`
	reqBody := bytes.NewReader([]byte("foo"))
	resp, err := http.Post(s.srv.URL+"/foo", "text/plain", reqBody)
	s.Require().NoError(err)
	s.Require().Equal(http.StatusOK, resp.StatusCode)
	s.Require().Equal("text/plain; charset=utf-8", resp.Header.Get("Content-Type"))
	resBody, err := io.ReadAll(resp.Body)
	s.Require().NoError(err)
	s.Require().Equal(want, string(resBody))
}

func (s *AppTestSuite) TestBar() {
	want := `bar`
	reqBody := bytes.NewReader([]byte("bar"))
	resp, err := http.Post(s.srv.URL+"/bar", "text/plain", reqBody)
	s.Require().NoError(err)
	s.Require().Equal(http.StatusOK, resp.StatusCode)
	s.Require().Equal("text/plain; charset=utf-8", resp.Header.Get("Content-Type"))
	resBody, err := io.ReadAll(resp.Body)
	s.Require().NoError(err)
	s.Require().Equal(want, string(resBody))
}

func (s *AppTestSuite) TestUnknownPath() {
	want := `{"error":"path does not match with any service"}`
	reqBody := bytes.NewReader([]byte("foobar"))
	resp, err := http.Post(s.srv.URL+"/foobar", "text/plain", reqBody)
	s.Require().NoError(err)
	s.Require().Equal(http.StatusNotFound, resp.StatusCode)
	s.Require().Equal("application/json", resp.Header.Get("Content-Type"))
	resBody, err := io.ReadAll(resp.Body)
	s.Require().NoError(err)
	s.Require().JSONEq(want, string(resBody))
}

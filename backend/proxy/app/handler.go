package app

import (
	"net"
	"net/http"
	"net/http/httputil"
	"net/url"
	"qubengage/proxy/config"
	"strings"
	"time"

	"github.com/go-chi/chi/v5"
)

func handle(r chi.Router, cfg *config.Config) error {
	for _, svc := range cfg.Services {
		proxy, err := makeProxy(svc)
		r.HandleFunc(svc.Path+"/*", func(w http.ResponseWriter, r *http.Request) {
			proxy.ServeHTTP(w, r)
		})
		r.HandleFunc(svc.Path+"/", func(w http.ResponseWriter, r *http.Request) {
			proxy.ServeHTTP(w, r)
		})
		r.HandleFunc(svc.Path, func(w http.ResponseWriter, r *http.Request) {
			proxy.ServeHTTP(w, r)
		})
		if err != nil {
			return err
		}
	}
	return nil
}

func makeProxy(svc *config.Service) (http.Handler, error) {
	uri, err := url.Parse(svc.Addr)
	if err != nil {
		return nil, err
	}
	proxy := &httputil.ReverseProxy{Director: func(req *http.Request) {
		originHost := uri.Host
		req.Header.Add("X-Forwarded-Host", req.Host)
		req.Header.Add("X-Origin-Host", originHost)
		req.URL.Host = originHost
		req.URL.Path = strings.TrimPrefix(req.URL.Path, svc.Path)
		req.URL.Scheme = uri.Scheme
		req.URL.RawQuery = uri.Query().Encode()
	}, Transport: &http.Transport{
		Dial: (&net.Dialer{
			Timeout: 5 * time.Second,
		}).Dial,
	}}

	return proxy, nil
}

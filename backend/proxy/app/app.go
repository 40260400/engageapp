package app

import (
	"errors"
	"log/slog"
	"net/http"
	"qubengage/proxy/config"
	"time"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/httplog/v2"
)

func Load(cfg *config.Config) (http.Handler, error) {
	r := chi.NewMux()
	logger := httplog.NewLogger("proxy", httplog.Options{
		LogLevel:         slog.LevelDebug,
		Concise:          true,
		RequestHeaders:   true,
		MessageFieldName: "message",
		TimeFieldFormat:  time.RFC850,
	})
	r.Use(httplog.RequestLogger(logger))

	err := handle(r, cfg)
	if err != nil {
		return nil, err
	}
	r.NotFound(func(w http.ResponseWriter, r *http.Request) {
		respondError(w, http.StatusNotFound, errors.New("path does not match with any service"))
	})
	return r, nil
}

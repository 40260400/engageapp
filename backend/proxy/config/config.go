package config

import (
	"fmt"
	"os"

	"gopkg.in/yaml.v3"
)

type Service struct {
	Path string `yaml:"path"`
	Addr string `yaml:"addr"`
}

type Config struct {
	Services []*Service `yaml:"services"`
}

func Load(data []byte) (*Config, error) {
	var cfg Config
	err := yaml.Unmarshal([]byte(data), &cfg)
	if err != nil {
		return nil, fmt.Errorf("malformed config yaml: %w", err)
	}
	return &cfg, nil
}

func LoadFromFile(fp string) (*Config, error) {
	data, err := os.ReadFile(fp)
	if err != nil {
		return nil, fmt.Errorf("failed to open '%s': %w", fp, err)
	}
	return Load(data)
}

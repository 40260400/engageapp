const express = require('express');
const parser = require('body-parser')
const cors = require('cors')
const handler = require('./handler');

const app = express();

app.use(cors())
app.use(parser.json());
app.use(parser.urlencoded({extended: true}));

app.post('/', handler);

module.exports = app;


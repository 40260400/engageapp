const validator = require('./validator');

module.exports = (req, res) => {
    const data = req.body;
    // validate data
    const err = validator.validateData(data);
    if (err) {
        res.status(400)
        res.json({error: err});
        return;
    }
    const cutoff = parseFloat(data.cutoff);
    const score = parseFloat(data.score);
    // check if student risks
    res.send({risk: cutoff > score});
};
function validateData(data) {
    if (!data || typeof data !== 'object') {
        return 'invalid request body';
    }
    
    if (!('cutoff' in data) || !('score' in data) ) {
        return `data is missing parameters`;
    }
    const err = validateScore(data.score);
    if (err) {
        return err;
    }
    return validateCutoff(data.cutoff);;
}

function validateCutoff(cutoff) {
    try {
        const cutoffNum = parseFloat(cutoff);
        if (!(0 < cutoffNum && cutoffNum <= 1)) {
            return `invalid cutoff size`;
        }
    } catch (error) {
        return `invalid cutoff is not set or has an invalid type`;
    }
    return null;
}
function validateScore(score) {
    try {
        const scoreNum = parseFloat(score);
        if (!(0 < scoreNum && scoreNum <= 1)) {
            return `invalid score size`;
        }
    } catch (error) {
        return `invalid score is not set or has an invalid type`;
    }
    return null;
}


module.exports = {
    validateData,
    validateCutoff,
    validateScore,
};
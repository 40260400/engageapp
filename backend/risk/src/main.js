const app = require('./app');
const port = 80;

app.listen(port, () => {
    console.log(`risk listening on port ${port}`)
});
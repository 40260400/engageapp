const app = require("../src/app");
const supertest = require("supertest");
const request = supertest(app);

describe("integration tests", () => {
    it("POST / should fail on score validation", async () => {
        const params = new URLSearchParams([
            ['score', 'wrongg',],
            ['cutoff', '0.3',],
        ]);
        const res = await request
            .post("/")
            .send(params.toString())
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Accept", "application/json");
        expect(res.status).toEqual(400);
        expect(res.type).toEqual("application/json");
        expect(res.body).toEqual({"error": "invalid score size"});
    });
    it("POST / should fail on missing property", async () => {
        const params = new URLSearchParams([
            ['score', '0.2',],
        ]);
        const res = await request
            .post("/")
            .send(params.toString())
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Accept", "application/json");
        expect(res.status).toEqual(400);
        expect(res.type).toEqual("application/json");
        expect(res.body).toEqual({"error": "data is missing parameters"});
    });
    it("POST / should return risk=true", async () => {
        const params = new URLSearchParams([
            ['score', '0.20',],
            ['cutoff', '0.25',],
        ]);
        const res = await request
            .post("/")
            .send(params.toString())
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Accept", "application/json");
        expect(res.status).toEqual(200);
        expect(res.type).toEqual("application/json");
        expect(res.body).toEqual({"risk": true});
    });
    it("POST / should return risk=false", async () => {
        const params = new URLSearchParams([
            ['score', '0.30',],
            ['cutoff', '0.15',],
        ]);
        const res = await request
            .post("/")
            .send(params.toString())
            .set("Content-Type", "application/x-www-form-urlencoded")
            .set("Accept", "application/json");
        expect(res.status).toEqual(200);
        expect(res.type).toEqual("application/json");
        expect(res.body).toEqual({"risk": false});
    });
});

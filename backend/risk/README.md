# backend/risk

Risk of Student Failure

## Development

### Prerequisites

- `node`
- `npm`

### Install dependencies

```sh
npm install
```

### Run tests

```sh
npm test
```
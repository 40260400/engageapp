from flask import (
    Blueprint,request, jsonify
)
from . import validator

bp = Blueprint('handler', __name__)

def parse_form_data(request_form):
    items = []
    for key in sorted(request_form.keys()):
        parts = key.split('][')
        idx = int(parts[0].split('[')[-1])
        attr = parts[1][:-1]

        while len(items) <= idx:
            items.append({})

        items[idx][attr] = request_form[key]

    return {"items": items}

@bp.route('/', methods=['POST'])
def handle():
    try:
        # Get form data from the request
        data = parse_form_data(request.form)

        # Validate the request body
        validation_error = validator.validate_data(data)
        if validation_error:
            return jsonify({"error": validation_error}), 400
        # Sum
        total_sum = 0
        for item in data['items']:
            total_sum += int(item['attendance'])
        # Return the result in JSON format
        return jsonify({"sum": total_sum})

    except Exception as e:
        return jsonify({"error": str(e)})


def validate_data(data):
    if not data or not isinstance(data, dict):
        return 'invalid request body'
    if 'items' not in data or not isinstance(data['items'], list):
        return 'missing or invalid items array'
    total = len(data['items'])
    if total == 0:
        return 'empty items array'
    elif total > 100:
        return 'too big items array'

    for idx, item in enumerate(data['items']):
        err = validate_item(item, idx)
        if err:
            return err

    return None

def validate_item(item, idx):
    if not item or not isinstance(item, dict):
        return f'item[{idx}] is not an object'
    if 'title' not in item or 'attendance' not in item or 'total' not in item or 'weight' not in item:
        return f'item[{idx}] is missing parameters'

    title_err = validate_title(item['title'], idx)
    if title_err:
        return title_err

    total_err = validate_total(item['total'], idx)
    if total_err:
        return total_err

    attendance_err = validate_attendance(item['attendance'], int(item['total']), idx)
    if attendance_err:
        return attendance_err

    weight_err = validate_weight(item['weight'], idx)
    if weight_err:
        return weight_err

    return None

def validate_title(title, idx):
    if not title or not isinstance(title, str):
        return f'invalid item[{idx}] title is not set or has an invalid type'
    length = len(title)
    if length <= 0 or length > 100:
        return f'invalid item[{idx}] title length'
    return None

def validate_attendance(attendance, total, idx):
    if not isinstance(attendance, str) or not attendance.isdigit():
        return f'invalid item[{idx}] attendance is not set or has an invalid type'
    attendance = int(attendance)
    if not 0 < attendance <= 100:
        return f'invalid item[{idx}] attendance size'
    if attendance > total:
        return f'item[{idx}] attendance size is bigger than total allowed'
    return None

def validate_weight(weight, idx):
    try:
        weight = float(weight)
        if not 0 < weight <= 1:
            return f'invalid item[{idx}] weight size'
    except ValueError:
        return f'invalid item[{idx}] weight is not set or has an invalid type'
    return None

def validate_total(total, idx):
    if not isinstance(total, str) or not total.isdigit():
        return f'invalid item[{idx}] total is not set or has an invalid type'
    total = int(total)
    if not 0 < total <= 100:
        return f'invalid item[{idx}] total size'
    return None

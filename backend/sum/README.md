# backend/sum

Total Attendance Hours

## Development

### Prerequisites

- `python3`
- `pip`

### Install dependencies

```sh
python3 -m venv .venv
. .venv/bin/activate
pip install -r requirements.txt
```

### Run tests

```sh
pytest -v
```

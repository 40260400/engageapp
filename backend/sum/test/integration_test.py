import pytest
import requests


def test_integration_successful_call(client):
    data = {
        'items[0][title]': 'Lecture sessions',
        'items[0][attendance]': '1',
        'items[0][weight]': '0.3',
        'items[0][total]': '33',
        'items[1][title]': 'Lab sessions',
        'items[1][attendance]': '2',
        'items[1][weight]': '0.4',
        'items[1][total]': '22',
        'items[2][title]': 'Support sessions',
        'items[2][attendance]': '3',
        'items[2][weight]': '0.15',
        'items[2][total]': '44',
        'items[3][title]': 'Canvas activities',
        'items[3][attendance]': '4',
        'items[3][weight]': '0.15',
        'items[3][total]': '55',
    }

    response = client.post('/', data=data)
    assert response.status_code == 200
    assert response.json == {"sum": 10}

def test_integration_error_missing_title(client):
    data = {
        'items[0][attendance]': '1',
        'items[0][weight]': '0.3',
        'items[0][total]': '33',
    }

    response = client.post('/', data=data)
    assert response.status_code == 400
    assert response.json == {"error": "item[0] is missing parameters"}

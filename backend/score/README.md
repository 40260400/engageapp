# backend/score

Student Engagement Score

## Development

### Prerequisites

- `go`

### Install dependencies

```sh
go mod download
```

### Run tests

```sh
go test ./...
```

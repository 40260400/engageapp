package main

import (
	"fmt"
	"net/http"
	"qubengage/score/app"
	"time"
)

func main() {
	srv := http.Server{
		Addr:         ":80",
		WriteTimeout: time.Second * 5,
		Handler:      app.New(),
	}
	if err := srv.ListenAndServe(); err != nil {
		fmt.Printf("server failed: %s\n", err)
	}
}

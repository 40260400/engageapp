package app

import (
	"fmt"
)

func validateData(data *RequestData) error {
	if data.Items == nil || len(data.Items) == 0 {
		return fmt.Errorf("empty items array")
	} else if len(data.Items) > 100 {
		return fmt.Errorf("too big items array")
	}

	for idx, item := range data.Items {
		if err := validateItem(item, idx); err != nil {
			return err
		}
	}

	return nil
}

func validateItem(item *Item, idx int) error {
	if item == nil {
		return fmt.Errorf("item[%d] is not an object", idx)
	}
	if item.Title == "" || item.Attendance == 0 || item.Total == 0 || item.Weight == 0 {
		return fmt.Errorf("item[%d] is missing parameters", idx)
	}

	if err := validateTitle(item.Title, idx); err != nil {
		return err
	}
	if err := validateTotal(item.Total, idx); err != nil {
		return err
	}
	if err := validateAttendance(item.Attendance, item.Total, idx); err != nil {
		return err
	}
	if err := validateWeight(item.Weight, idx); err != nil {
		return err
	}

	return nil
}

func validateTitle(title string, idx int) error {
	if title == "" || len(title) <= 0 || len(title) > 100 {
		return fmt.Errorf("invalid item[%d] title is not set or has an invalid type", idx)
	}
	return nil
}

func validateAttendance(attendance int, total int, idx int) error {
	if attendance <= 0 || attendance > 100 {
		return fmt.Errorf("invalid item[%d] attendance is not set or has an invalid type", idx)
	}
	if attendance > total {
		return fmt.Errorf("item[%d] attendance size is bigger than total allowed", idx)
	}
	return nil
}

func validateWeight(weight float64, idx int) error {
	if weight <= 0 || weight > 1 {
		return fmt.Errorf("invalid item[%d] weight size", idx)
	}
	return nil
}

func validateTotal(total int, idx int) error {
	if total <= 0 || total > 100 {
		return fmt.Errorf("invalid item[%d] total size", idx)
	}
	return nil
}

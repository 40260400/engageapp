package app

import (
	"errors"
	"net/http"
)

type Item struct {
	Title      string  `json:"title"`
	Attendance int     `json:"attendance"`
	Total      int     `json:"total"`
	Weight     float64 `json:"weight"`
}

type RequestData struct {
	Items []*Item `json:"items"`
}

type ResponseData struct {
	Score float64 `json:"score"`
}

func handleRoot(w http.ResponseWriter, r *http.Request) {
	// check method
	if r.Method != http.MethodPost {
		respondError(w, http.StatusMethodNotAllowed, errors.New("method not allowed"))
		return

	}
	// parse form data
	err := r.ParseForm()
	if err != nil {
		respondError(w, http.StatusInternalServerError, err)
		return
	}
	data, err := parseForm(r.PostForm)
	if err != nil {
		respondError(w, http.StatusBadRequest, err)
		return
	}
	// validate request data
	err = validateData(data)
	if err != nil {
		respondError(w, http.StatusBadRequest, err)
		return
	}

	// calculate the engagement score
	var score float64
	for _, item := range data.Items {
		score += (float64(item.Attendance) * item.Weight) / float64(item.Total)
	}

	respondJSON(w, &ResponseData{
		Score: score,
	})
}

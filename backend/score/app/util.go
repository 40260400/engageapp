package app

import (
	"encoding/json"
	"net/http"
)

func respondError(w http.ResponseWriter, status int, err error) {
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(status)
	enc := json.NewEncoder(w)
	enc.Encode(map[string]string{
		"error": err.Error(),
	})
}

func respondJSON(w http.ResponseWriter, data any) {
	w.Header().Add("Content-Type", "application/json")
	enc := json.NewEncoder(w)
	enc.Encode(data)
}

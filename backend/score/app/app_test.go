package app

import (
	"bytes"
	"context"
	"io"
	"net/http"
	"net/http/httptest"
	"net/url"
	"testing"

	"github.com/stretchr/testify/suite"
)

type AppTestSuite struct {
	suite.Suite
	srv    *httptest.Server
	ctx    context.Context
	cancel context.CancelFunc
}

func (s *AppTestSuite) BeforeTest(_, _ string) {
	s.ctx, s.cancel = context.WithCancel(context.Background())
	s.srv = httptest.NewServer(New())
}

func (s *AppTestSuite) TearDownTest() {
	s.cancel()
}

func (s *AppTestSuite) AfterTest(_, _ string) {
	s.srv.Close()
}

func TestAppTestSuite(t *testing.T) {
	suite.Run(t, new(AppTestSuite))
}

func (s *AppTestSuite) TestSuccess() {
	want := `{"score":0.0665909108338934}`
	params := make(url.Values)
	params.Set("items[0][title]", "Lecture sessions")
	params.Set("items[0][attendance]", "1")
	params.Set("items[0][weight]", "0.3")
	params.Set("items[0][total]", "33")
	params.Set("items[1][title]", "Lab sessions")
	params.Set("items[1][attendance]", "2")
	params.Set("items[1][weight]", "0.4")
	params.Set("items[1][total]", "22")
	params.Set("items[2][title]", "Support sessions")
	params.Set("items[2][attendance]", "3")
	params.Set("items[2][weight]", "0.15")
	params.Set("items[2][total]", "44")
	params.Set("items[3][title]", "Canvas activities")
	params.Set("items[3][attendance]", "4")
	params.Set("items[3][weight]", "0.15")
	params.Set("items[3][total]", "55")
	reqBody := bytes.NewReader([]byte(params.Encode()))
	resp, err := http.Post(s.srv.URL+"/", "application/x-www-form-urlencoded", reqBody)
	s.Require().NoError(err)
	s.Require().Equal(http.StatusOK, resp.StatusCode)
	s.Require().Equal("application/json", resp.Header.Get("Content-Type"))
	resBody, err := io.ReadAll(resp.Body)
	s.Require().NoError(err)
	s.Require().JSONEq(want, string(resBody))
}

func (s *AppTestSuite) TestInvalidData() {
	want := `{"error":"item[0] is missing parameters"}`
	params := make(url.Values)
	params.Set("items[0][attendance]", "1")
	params.Set("items[0][weight]", "0.3")
	params.Set("items[0][total]", "33")
	reqBody := bytes.NewReader([]byte(params.Encode()))
	resp, err := http.Post(s.srv.URL+"/", "application/x-www-form-urlencoded", reqBody)
	s.Require().NoError(err)
	s.Require().Equal(http.StatusBadRequest, resp.StatusCode)
	s.Require().Equal("application/json", resp.Header.Get("Content-Type"))
	resBody, err := io.ReadAll(resp.Body)
	s.Require().NoError(err)
	s.Require().JSONEq(want, string(resBody))
}

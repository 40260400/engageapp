package app

import (
	"errors"
	"net/url"
	"sort"
	"strconv"
	"strings"
)

func parseForm(values url.Values) (*RequestData, error) {
	itemMap := make(map[string]*Item, 0)

	keys := make([]string, 0)
	for key := range values {
		keys = append(keys, key)
	}
	sort.Strings(keys)

	for _, key := range keys {
		parts := strings.Split(strings.TrimSuffix(strings.TrimPrefix(key, "items["), "]"), "][")
		if len(parts) != 2 {
			return nil, errors.New("invalid form values")
		}
		idx := parts[0]
		attr := parts[1]
		item, ok := itemMap[idx]
		if !ok {
			item = new(Item)
			itemMap[idx] = item
		}
		switch attr {
		case "title":
			item.Title = values.Get(key)
		case "attendance":
			n, _ := strconv.Atoi(values.Get(key))
			item.Attendance = n
		case "total":
			n, _ := strconv.Atoi(values.Get(key))
			item.Total = n
		case "weight":
			n, _ := strconv.ParseFloat(values.Get(key), 32)
			item.Weight = float64(n)
		}

	}
	items := []*Item{}
	for _, item := range itemMap {
		items = append(items, item)
	}
	return &RequestData{
		Items: items,
	}, nil
}

# backend

## Services

- [maxmin](maxmin/) - Find Maximum and Minimum Attendance
- [risk](risk/) - Risk of Student Failure
- [score](score/) - Student Engagement Score
- [sort](sort/) - Sort Attendance
- [sum](sum/) - Total Attendance Hours
- [recognition](recognition/) - Student Recognition
- [proxy](proxy/) - Proxy Server

package site.jmproject.recognition;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import site.jmproject.recognition.controllers.RootController;

@SpringBootTest
class RecognitionApplicationTests {

	@Autowired
	private RootController controller;

	@Test
	void contextLoads() throws Exception {
		assertNotNull(controller);
	}

}

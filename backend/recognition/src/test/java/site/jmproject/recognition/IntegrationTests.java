package site.jmproject.recognition;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import site.jmproject.recognition.dto.ErrorResponse;
import site.jmproject.recognition.dto.ResponseData;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class IntegrationTests {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    String apiURL() {
        return "http://localhost:" + port + "/";
    }

    @Test
    void shouldNotRecognizeBelowThreshold() throws Exception {
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("Content-Type", MediaType.APPLICATION_FORM_URLENCODED_VALUE);
        headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);
        ResponseEntity<ResponseData> res = this.restTemplate.exchange(
                this.apiURL(),
                HttpMethod.POST,
                new HttpEntity<String>("score=0.1", headers),
                ResponseData.class);
        assertEquals(HttpStatus.OK, res.getStatusCode());
        assertFalse(res.getBody().isRecognized());
    }

    @Test
    void shouldNotRecognizeAtThreshold() throws Exception {
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("Content-Type", MediaType.APPLICATION_FORM_URLENCODED_VALUE);
        headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);
        ResponseEntity<ResponseData> res = this.restTemplate.exchange(
                this.apiURL(),
                HttpMethod.POST,
                new HttpEntity<String>("score=0.9", headers),
                ResponseData.class);
        assertEquals(HttpStatus.OK, res.getStatusCode());
        assertTrue(res.getBody().isRecognized());
    }

    @Test
    void shouldNotRecognizeAboveThreshold() throws Exception {
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("Content-Type", MediaType.APPLICATION_FORM_URLENCODED_VALUE);
        headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);
        ResponseEntity<ResponseData> res = this.restTemplate.exchange(
                this.apiURL(),
                HttpMethod.POST,
                new HttpEntity<String>("score=0.95", headers),
                ResponseData.class);
        assertEquals(HttpStatus.OK, res.getStatusCode());
        assertTrue(res.getBody().isRecognized());
    }

    @Test
    void shouldErrorValidation() throws Exception {
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("Content-Type", MediaType.APPLICATION_FORM_URLENCODED_VALUE);
        headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);
        ResponseEntity<ErrorResponse> res = this.restTemplate.exchange(
                this.apiURL(),
                HttpMethod.POST,
                new HttpEntity<String>("score=10", headers),
                ErrorResponse.class);
        assertEquals(HttpStatus.BAD_REQUEST, res.getStatusCode());
        assertEquals("score cannot exceed 1.0", res.getBody().getError());
    }
}
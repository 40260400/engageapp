package site.jmproject.recognition.controllers;

import org.springframework.web.bind.annotation.RestController;

import site.jmproject.recognition.dto.ErrorResponse;
import site.jmproject.recognition.dto.RequestData;
import site.jmproject.recognition.dto.ResponseData;
import site.jmproject.recognition.exceptions.ValidationException;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.PostMapping;

@RestController
public class RootController {

    final float threshold =  (float) 0.9;

    @PostMapping(
        path = "/", 
        consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE, 
        produces = {
          MediaType.APPLICATION_JSON_VALUE
        }
    )
    @CrossOrigin(origins = "*")
    public @ResponseBody ResponseData handleRoot( RequestData req) throws ValidationException {
        float score = 0;
        boolean recognized = false;
        if (req.getScore() == null || req.getScore().trim() == "") {
            throw new ValidationException("missing score value");
        }
        try {
            score = Float.parseFloat(req.getScore());
        } catch(Exception err) {
            throw new ValidationException("invalid score value");
        }
        if (score < 0) {
            throw new ValidationException("score cannot be negative");
        } else if (score > 1) {
            throw new ValidationException("score cannot exceed 1.0");
        }
        if (score >= threshold) {
            recognized = true;
        } 
        return new ResponseData(recognized);
    }
    
    @ExceptionHandler(ValidationException.class) 
    public ResponseEntity<ErrorResponse> handleExceptions(
        ValidationException e
    ) {
        HttpStatus status = HttpStatus.BAD_REQUEST;
        return new ResponseEntity<>(
            new ErrorResponse(e.getMessage()),
            status
        );
    }
    @ExceptionHandler(Exception.class) 
    public ResponseEntity<ErrorResponse> handleExceptions(
        Exception e
    ) {
        HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR; 
        return new ResponseEntity<>(
            new ErrorResponse(e.getMessage()),
            status
        );
    }
}

package site.jmproject.recognition.dto;

import java.io.Serializable;


import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class ResponseData implements Serializable {
    boolean recognized;
    public ResponseData(boolean recognized) {
        this.recognized = recognized;
    }
    public ResponseData () {}   
}

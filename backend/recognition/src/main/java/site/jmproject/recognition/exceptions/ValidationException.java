package site.jmproject.recognition.exceptions;

import lombok.Getter;

public class ValidationException extends Exception{
    @Getter
    String message;
    public ValidationException(String message) {
        this.message = message;
    }
}

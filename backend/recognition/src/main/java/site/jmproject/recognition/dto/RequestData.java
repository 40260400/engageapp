package site.jmproject.recognition.dto;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class RequestData implements Serializable {
    String score;
    public RequestData (String score) {
        this.score = score;
    }
    public RequestData () {}    
}

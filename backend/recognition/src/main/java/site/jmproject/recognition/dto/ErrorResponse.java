package site.jmproject.recognition.dto;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class ErrorResponse implements Serializable {
    String error;
    public ErrorResponse() {}
    public ErrorResponse(String error) {
        this.error = error;
    }
}

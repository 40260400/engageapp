<?php

namespace App;

use InvalidArgumentException;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Slim\Routing\RouteContext;
use Slim\Factory\AppFactory;
use Throwable;

class App
{
    /**
     * Stores Slim application.
     *
     * @var \Slim\App
     */
    private $app;

    public function __construct() {
       
        $app = AppFactory::create();

        $app->addBodyParsingMiddleware();

        // CORS
        $app->add(function (Request $request, RequestHandlerInterface $handler): Response {
            $routeContext = RouteContext::fromRequest($request);
            $routingResults = $routeContext->getRoutingResults();
            $methods = $routingResults->getAllowedMethods();
            $requestHeaders = $request->getHeaderLine('Access-Control-Request-Headers');

            $response = $handler->handle($request);

            $response = $response->withHeader('Access-Control-Allow-Origin', '*');
            $response = $response->withHeader('Access-Control-Allow-Methods', implode(',', $methods));
            $response = $response->withHeader('Access-Control-Allow-Headers', $requestHeaders);

            return $response;
        });


        $app->addRoutingMiddleware();

        // Register Action route
        $app->post('/', Action::class);

        // Register options route for CORS
        $app->options('/', function ($request, $response, $args) {
            return $response;
        });

        // Register error handlers

        $errors = $app->addErrorMiddleware(true, true, true);

        $errors->setErrorHandler(InvalidArgumentException::class, function (
            ServerRequestInterface $request,
            Throwable $exception,
            bool $displayErrorDetails,
            bool $logErrors,
            bool $logErrorDetails
        ) {
            $response = new \Slim\Psr7\Response();
            $response->getBody()->write(
                json_encode([
                'error' => $exception->getMessage(),
                ]),
            );
            return $response
                ->withStatus(400)
                ->withHeader('Access-Control-Allow-Origin', '*');
        });

        $this->app = $app;
    }

    /**
     * Get an instance of the application.
     *
     * @return \Slim\App
     */
    public function get()
    {
        return $this->app;
    }
}
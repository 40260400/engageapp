<?php

namespace App;

use InvalidArgumentException;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class Action
{
   public function __invoke(Request $request, Response $response, array $args): Response {
        // Parse request
        $data = $request->getParsedBody();
        // Validate request data
        $err = Validator::validateData($data);
        if ($err != null) {
            throw new InvalidArgumentException($err);
        }
        // Sort items
        $items = $data['items'];
        usort($items, function($a, $b) {
            return $b['attendance'] <=> $a['attendance'];
        });
        // Respond
        $response->getBody()->write(
            json_encode([
                'items' => $items,
            ]),
        );
        return $response->withHeader('Content-Type', 'application/json'); 
   }
}
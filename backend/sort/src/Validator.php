<?php

namespace App;

class Validator
{
    public static function validateData($data) {
        if (!is_array($data)) {
            return 'invalid request body';
        }
        if (!is_array($data['items'])) {
            return 'missing items';
        }
        $total = count($data['items']);
        if ($total == 0) {
            return 'empty items array';
        }else if ($total > 100) {
            return 'too big items array';
        }
        $idx = 0;
        foreach ($data['items'] as $item)
         {
            $err = Validator::validateItem($item, $idx);
            if ($err != null) {
                return $err;
            }
            $idx++;
        } 
        return null;
    }
    
    public static function validateItem($item, $idx) {
        if (!is_array($item)) {
            return 'item['.$idx.'] is not an object';
        }
        if (!isset($item['title']) || !isset($item['attendance']) ||!isset($item['total']) ||!isset($item['weight'])) {
            return 'item['.$idx.'] is missing parameters';
        }
        $err = Validator::validateTitle($item['title'], $idx);
        if ($err != null) {
            return $err;
        }
        $err = Validator::validateTotal($item['total'], $idx);
        if ($err != null) {
            return $err;
        }
        $err = Validator::validateAttendance($item['attendance'], $item['total'], $idx);
        if ($err != null) {
            return $err;
        }
        $err = Validator::validateWeight($item['weight'], $idx);
        if ($err != null) {
            return $err;
        }
        return null;
    }
    
    public static function validateTitle($title, $idx) {
        if (!isset($title) || $title == '') {
            return 'invalid item['.$idx.'] title is not set';
        }
        if (!is_string($title)) {
            return 'invalid item['.$idx.'] title type';
        }
        $len = strlen($title);
        if ($len <= 0 || $len > 100) {
            return 'invalid item['.$idx.'] title length';
        }
        return null;
    }
    
    public static function validateAttendance($attendance, $total, $idx) {
        if (!isset($attendance) || $attendance == '') {
            return 'invalid item['.$idx.'] attendance is not set';
        }
        if (!is_numeric($attendance)) {
            return 'invalid item['.$idx.'] attendance type';
        }
        if ($attendance <= 0 || $attendance > 100) {
            return 'invalid item['.$idx.'] attendance size';
        }
        if ($attendance > $total) {
            return 'item['.$idx.'] attendance size bigger than total allowed';
        }
        return null;
    }

    public static function validateWeight($weight, $idx) {
        if (!isset($weight) || $weight == '') {
            return 'invalid item['.$idx.'] weight is not set';
        }
        if (!is_numeric($weight)) {
            return 'invalid item['.$idx.'] weight type';
        }
        if ($weight <= 0 || $weight > 1) {
            return 'invalid item['.$idx.'] weight size';
        }
        return null;
    }

    public static function validateTotal($total, $idx) {
        if (!isset($total) || $total == '') {
            return 'invalid item['.$idx.'] total is not set';
        }
        if (!is_numeric($total)) {
            return 'invalid item['.$idx.'] total type';
        }
        if ($total <= 0 || $total > 100) {
            return 'invalid item['.$idx.'] total size';
        }
        return null;
    }
}

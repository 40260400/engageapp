# backend/maxmin

Find Maximum and Minimum Attendance

## Development

### Prerequisites

- `php`
- `composer`

### Install dependencies

```sh
composer install
```

### Run tests

```sh
vendor/bin/phpunit tests
```

<?php

namespace App;

use InvalidArgumentException;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class Action
{
   public function __invoke(Request $request, Response $response, array $args): Response {
        // Parse request
        $data = $request->getParsedBody();
        // Validate request data
        $err = Validator::validateData($data);
        if ($err != null) {
            throw new InvalidArgumentException($err);
        }
        // Find min and max values
        $min = null;
        $max = null;
        foreach ($data['items'] as $item) {
            if ($min == null || $item['attendance'] < $min['attendance']) {
                $min = $item;
            }
            if ($max == null || $item['attendance'] > $max['attendance']) {
                $max = $item;
            }
        }
        // Respond
        $response->getBody()->write(
            json_encode([
                'min' => $min,
                'max' => $max,
            ]),
        );
        return $response->withHeader('Content-Type', 'application/json'); 
   }
}
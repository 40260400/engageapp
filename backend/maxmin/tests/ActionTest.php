<?php
use App\App;
use PHPUnit\Framework\TestCase;
use Slim\Psr7\Environment;
use Slim\Psr7\Factory\ResponseFactory;
use Slim\Psr7\Factory\StreamFactory;
use Slim\Psr7\Factory\UriFactory;
use Slim\Psr7\Headers;
use Slim\Psr7\Request;
use Slim\Psr7\Uri;

class ActionTest extends TestCase
{
    protected $app;

    public function setUp(): void
    {
        // Instantiate your Slim\App
        $this->app = (new App())->get();
    }

    public function testSuccessful()
    {
        // Create Slim PSR-7 Request
        $uriFactory = new UriFactory();
        $uri = $uriFactory->createUri('/');
        $headers = new Headers([
            'Content-Type' => 'multipart/form-data',
        ]);
        $body = (new StreamFactory())->createStream();
        $request = new Request('POST', $uri, $headers, [], [], $body);

        // Add form parameters to simulate the form data
        $request = $request->withParsedBody([
            'items' => [
                [
                    'title' => 'Lecture sessions',
                    'attendance' => '1',
                    'weight' => '0.3',
                    'total' => '33',
                ],
                [
                    'title' => 'Lab sessions',
                    'attendance' => '2',
                    'weight' => '0.4',
                    'total' => '22',
                ],
                [
                    'title' => 'Support sessions',
                    'attendance' => '3',
                    'weight' => '0.15',
                    'total' => '44',
                ],
                [
                    'title' => 'Canvas activities',
                    'attendance' => '4',
                    'weight' => '0.15',
                    'total' => '55',
                ],
            ],
        ]);

        // Create a response
        $response = (new ResponseFactory())->createResponse();

        // Process the request through the Slim app
        $response = $this->app->handle($request);

        // Assert the status code and response body
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals(
            '{"min":{"title":"Lecture sessions","attendance":"1","weight":"0.3","total":"33"},'.
            '"max":{"title":"Canvas activities","attendance":"4","weight":"0.15","total":"55"}}',
            (string)$response->getBody(),
        );
    }
    public function testInvalidData()
    {
        // Create Slim PSR-7 Request
        $uriFactory = new UriFactory();
        $uri = $uriFactory->createUri('/');
        $headers = new Headers([
            'Content-Type' => 'multipart/form-data',
        ]);
        $body = (new StreamFactory())->createStream();
        $request = new Request('POST', $uri, $headers, [], [], $body);

        // Add form parameters to simulate the form data
        $request = $request->withParsedBody([
            'items' => [
                [
                    'attendance' => '1',
                    'weight' => '0.3',
                    'total' => '33',
                ],
            ],
        ]);

        // Create a response
        $response = (new ResponseFactory())->createResponse();

        // Process the request through the Slim app
        $response = $this->app->handle($request);

        // Assert the status code and response body
        $this->assertEquals(400, $response->getStatusCode());
        $this->assertEquals(
            '{"error":"item[0] is missing parameters"}',
            (string)$response->getBody(),
        );
    }

    // Add more test cases as needed
}
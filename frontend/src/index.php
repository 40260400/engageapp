<?php
    $maxminURL = getenv('MAXMIN_URL', true) ? getenv('MAXMIN_URL') : '';
    $sortURL = getenv('SORT_URL', true) ? getenv('SORT_URL') : '';
    $sumURL = getenv('SUM_URL', true) ? getenv('SUM_URL') : '';
    $scoreURL = getenv('SCORE_URL', true) ? getenv('SCORE_URL') : '';
    $riskURL = getenv('RISK_URL', true) ? getenv('RISK_URL') : '';
    $recognitionURL = getenv('RISK_URL', true) ? getenv('RECOGNITION_URL') : '';
    $proxyURL = getenv('PROXY_URL', true) ? getenv('PROXY_URL') : '';
    $proxyEnabled = getenv('PROXY_ENABLED', true) ? getenv('PROXY_ENABLED') === 'true' : false;
    if ($proxyEnabled) {
        $maxminURL =  $proxyURL.'maxmin';
        $sortURL =  $proxyURL.'sort';
        $sumURL =  $proxyURL.'sum';
        $scoreURL =  $proxyURL.'score';
        $riskURL =  $proxyURL.'risk';
        $recognitionURL = $proxyURL.'recognition';
    }
?> 
<!DOCTYPE html>
<html>
<head>
<title>QUBEngage</title>
<link rel="stylesheet" href="style.css?<? echo time();?>">
</head>
<body>
<div id="sem">
    <div id="logo">
        QUBEngage
    </div>
    <div id="input-div-1">

        <form id="data" action="javascript:void(0)">
            <div>
                <input name="items[0][title]" class="display-item" type="text" value="Lecture sessions">
                <input name="items[0][attendance]" class="display-attendance"  type="text" placeholder="00">
                <input name="items[0][weight]" type="hidden" value="0.3">
                <input name="items[0][total]" type="hidden" value="33">
                <label>/33 (hours)</label>
            </div>
            <div>
                <input name="items[1][title]" class="display-item" type="text" value="Lab sessions">
                <input name="items[1][attendance]" class="display-attendance"  type="text" placeholder="00">
                <input name="items[1][weight]" type="hidden" value="0.4">
                <input name="items[1][total]" type="hidden" value="22">
                <label>/22 (hours)</label>
            </div>
            <div>
                <input name="items[2][title]" class="display-item" type="text" value="Support sessions">
                <input name="items[2][attendance]" class="display-attendance"  type="text" placeholder="00">
                <input name="items[2][weight]" type="hidden" value="0.15">
                <input name="items[2][total]" type="hidden" value="44">
                <label>/44 (hours)</label>
            </div>
            <div>
                <input name="items[3][title]" class="display-item" type="text" value="Canvas activities">
                <input name="items[3][attendance]" class="display-attendance"  type="text" placeholder="00">
                <input name="items[3][weight]" type="hidden" value="0.15">
                <input name="items[3][total]" type="hidden" value="55">
                <label>/55 (hours)</label>
            </div>
        </form>
    </div>
    <div id="input-div-2">
        <label class="display-item" id="cutoff">Cut-off Engaggement Score</label>
        <input class="display-attendance"  type="text" id="cutoff-value" name="cutoff" placeholder="00"><label>/100 (%)</label>
    </div>
    <div id="output-div">
        <textarea class="display-output" id="output-text" rows="5" cols="35" readonly=1 placeholder="Results here..." value="">
        </textarea>
    </div>
    <div>
        <button class="sembutton-active" onclick="getMaxMin();">Maximum and Minimum Attendance</button>
    </div>
    <div>
        <button class="sembutton-active" onclick="getSortedAttendance();">Sort Attendance</button>
    </div>
    <div>
        <button class="sembutton-active" onclick="getTotal();">Total Attendance Hours</button>
    </div>
    <div>
        <button class="sembutton-active" onclick="getEngagementScore();">Student Engagement Score</button>
    </div>
    <div>
        <button class="sembutton-active" onclick="getRisk();">Risk of Student Failure</button>
    </div>
    <div>
        <button class="sembutton-active" onclick="getRecognition();">Student Recognition</button>
    </div>
    <div>
        <button class="sembutton-clear" onclick="clearText();">Clear</button>
    </div>

</div>
</body>

<script type="text/javascript">
    const maxminURL = "<? echo $maxminURL; ?>";
    const sortURL =  "<? echo $sortURL; ?>";
    const sumURL = "<? echo $sumURL; ?>";
    const scoreURL = "<? echo $scoreURL; ?>";
    const riskURL = "<? echo $riskURL; ?>";
    const recognitionURL = "<? echo $recognitionURL; ?>";
</script>
<script src="script.js?<? echo time();?>"></script>
</html>

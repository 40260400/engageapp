function displayTotal(sum) {
    document.getElementById("output-text").value =
        `Total Attendance (in hours) = ${sum} hours`;
}

function displayMaxMin(max, min) {
    const output = document.getElementById("output-text");
    output.value = [
        `Maximum attendance = ${max.title} - ${max.attendance} hours`,
        `Minimum attendance = ${min.title} - ${min.attendance} hours`,
    ].join("\r\n");
}

function displaySortedAttendance(items) {
    const output = document.getElementById("output-text");
    output.value = items
        .map((item) => `${item.title} - ${item.attendance} hours`)
        .join("\r\n");
}

function displayEngagementScore(score) {
    document.getElementById("output-text").value =
        `Engagement Score - ${Math.round(score * 10000)/100}%`;
}


function displayRisk(risk) {
    document.getElementById("output-text").value = [
        `Risk of Student Failure - ${risk ? 'YES' : 'NO'}`
    ];
}

function displayRecognition(recognized) {
    document.getElementById("output-text").value = [
        `Has student shown exceptional engagement? - ${recognized ? 'YES' : 'NO'}`
    ];
}

function clearText() {
    const inputs = document.querySelectorAll(
        "#data input.display-attendance, #output-text, #cutoff-value"
    );
    inputs.forEach((input) => (input.value = ""));
}

function getFormData() {
    const data = new FormData(document.getElementById("data"));
    return new URLSearchParams(data);
}

function getCutoff() {
    const val = document.getElementById("cutoff-value").value;
    try { 
        return parseFloat(val.trim()) / 100;
    } catch (err) {
        alert('invalid cutoff value')
    }
}

async function getMaxMin() {
    try {
        const res = await fetch(maxminURL, {
            body: getFormData(),
            method: "post",
            headers: {
              'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
            },
        });
        const data = await res.json();
        if (data.error) {
            throw new Error(data.error);
        }
        displayMaxMin(data.max, data.min);
    } catch (err) {
        console.error(err);
        alert(err.message);
    }
}

async function getSortedAttendance() {
    try {
        const res = await fetch(sortURL, {
            body: getFormData(),
            method: "post",
            headers: {
              'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
            },
        });
        const data = await res.json();
        if (data.error) {
            throw new Error(data.error);
        }
        displaySortedAttendance(data.items);
    } catch (err) {
        console.error(err);
        alert(err.message);
    }
}

async function getTotal() {
    try {
        const res = await fetch(sumURL, {
            body: getFormData(),
            method: "post",
            headers: {
              'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
            },
        });
        const data = await res.json();
        if (data.error) {
            throw new Error(data.error);
        }
        displayTotal(data.sum);
    } catch (err) {
        console.error(err);
        alert(err.message);
    }
}

async function getEngagementScore() {
    try {
        const res = await fetch(scoreURL, {
            body: getFormData(),
            method: "post",
            headers: {
              'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
            },
        });
        const data = await res.json();
        if (data.error) {
            throw new Error(data.error);
        }
        displayEngagementScore(data.score);
    } catch (err) {
        console.error(err);
        alert(err.message);
    }
}


async function getRisk() {
    try {
        let res = await fetch(scoreURL, {
            body: getFormData(),
            method: "post",
            headers: {
              'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
            },
        });
        let data = await res.json();
        if (data.error) {
            throw new Error(data.error);
        }
        res = await fetch(riskURL, {
            body: new URLSearchParams([
                ["score", data.score],
                ["cutoff", getCutoff()],
            ]),
            method: "post",
            headers: {
              'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
            },
        });
        data = await res.json();
        if (data.error) {
            throw new Error(data.error);
        }
        displayRisk(data.risk);
    } catch (err) {
        console.error(err);
        alert(err.message);
    }
}

async function getRecognition() {
    try {
        let res = await fetch(scoreURL, {
            body: getFormData(),
            method: "post",
            headers: {
              'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
            },
        });
        let data = await res.json();
        if (data.error) {
            throw new Error(data.error);
        }
        res = await fetch(recognitionURL, {
            body: new URLSearchParams([
                ["score", data.score],
            ]),
            method: "post",
            headers: {
              'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
            },
        });
        data = await res.json();
        if (data.error) {
            throw new Error(data.error);
        }
        displayRecognition(data.recognized);
    } catch (err) {
        console.error(err);
        alert(err.message);
    }
}
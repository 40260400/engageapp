# frontend

## Prerequisites

- `php`

## Configuration

Frontend component is configured using environment variables.

```txt:
MAXMIN_URL=http://localhost:8081/
SORT_URL=http://localhost:8082/
SUM_URL=http://localhost:8083/
SCORE_URL=http://localhost:8085/
RISK_URL=http://localhost:8084/
PROXY_URL=http://localhost:8888/
PROXY_ENABLED=false
```

> **Note:** If `PROXY_ENABLED=true` then frontend will use PROXY_URL to call other services.
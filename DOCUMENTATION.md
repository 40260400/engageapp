## Function One

### Function (What Operation):

Find Maximum and Minimum Attendance (maxmin)

### Repository URL:

[/backend/maxmin](/backend/maxmin/)

### Live Service URL:

http://maxmin.jmproject.site

### Description of Implementation (language, methods, paradigm, etc):

- Language - PHP
- Framework - Slim
- API:
    ```txt
    POST /
    Request Headers:
        Content-Type: application/x-www-form-urlencoded
    Request Body:
        items[0][title]: Lecture sessions
        items[0][attendance]: 1
        items[0][weight]: 0.3
        items[0][total]: 33
        items[1][title]: Lab sessions
        items[1][attendance]: 2
        items[1][weight]: 0.4
        items[1][total]: 22
        items[2][title]: Support sessions
        items[2][attendance]: 3
        items[2][weight]: 0.15
        items[2][total]: 44
        items[3][title]: Canvas activities
        items[3][attendance]: 4
        items[3][weight]: 0.15
        items[3][total]: 55

    Response Headers:
        Content-Type: application/json
    Response Body:
        {
            "min": {
                "title": "Lecture sessions",
                "attendance": "1",
                "weight": "0.3",
                "total": "33"
            },
            "max": {
                "title": "Canvas activities",
                "attendance": "4",
                "weight": "0.15",
                "total": "55"
            }
        }
    ```

### Description of Testing:

Two integration tests for successful and failure case.

Command:
```sh
$ vendor/bin/phpunit tests
```

### Anything else to highlight for Function One:

While refactoring changed request method to POST and moved request data from url query to request body.
Also, I noticed that sort was used in previous PHP implementation to find min & max values. That's not required for such a simple operation. Min & max values can be found by interacting just once:

```php
$min = null;
$max = null;
foreach ($data['items'] as $item) {
    if ($min == null || $item['attendance'] < $min['attendance']) {
        $min = $item;
    }
    if ($max == null || $item['attendance'] > $max['attendance']) {
        $max = $item;
    }
}
```

To start development use this command to install dependencies:
```sh
composer install
```
 
## Function Two

### Function (What Operation):

Sort Attendance (sort)

### Repository URL:

[/backend/sort](/backend/sort/)

### Live Service URL:

http://sort.jmproject.site

### Description of Implementation (language, methods, paradigm, etc):

- Language - PHP
- Framework - Slim
- API:
    ```txt
    POST /
    Request Headers:
        Content-Type: application/x-www-form-urlencoded
    Request Body:
        items[0][title]: Lecture sessions
        items[0][attendance]: 1
        items[0][weight]: 0.3
        items[0][total]: 33
        items[1][title]: Lab sessions
        items[1][attendance]: 2
        items[1][weight]: 0.4
        items[1][total]: 22
        items[2][title]: Support sessions
        items[2][attendance]: 3
        items[2][weight]: 0.15
        items[2][total]: 44
        items[3][title]: Canvas activities
        items[3][attendance]: 4
        items[3][weight]: 0.15
        items[3][total]: 55

    Response Headers:
        Content-Type: application/json
    Response Body:
        {
            "items": [
                {
                    "title": "Canvas activities",
                    "attendance": "4",
                    "weight": "0.15",
                    "total": "55"
                },
                {
                    "title": "Support sessions",
                    "attendance": "3",
                    "weight": "0.15",
                    "total": "44"
                },
                {
                    "title": "Lab sessions",
                    "attendance": "2",
                    "weight": "0.4",
                    "total": "22"
                },
                {
                    "title": "Lecture sessions",
                    "attendance": "1",
                    "weight": "0.3",
                    "total": "33"
                }
            ]
        }
    ```

### Description of Testing:

Two integration tests for successful and failure case.

Command:
```sh
$ vendor/bin/phpunit tests
```

### Anything else to highlight for Function Two:

While refactoring changed request method to POST and moved request data from url query to request body.

To start development use this command to install dependencies:
```sh
composer install
```

## Function Three

### Function (What Operation):

Total Attendance Hours (sum)

### Repository URL:

[/backend/sum](/backend/sum/)

### Live Service URL:

http://sum.jmproject.site

### Description of Implementation (language, methods, paradigm, etc):

- Language - Python
- Framework - Flask
- API:
    ```txt
    POST /
    Request Headers:
        Content-Type: application/x-www-form-urlencoded
    Request Body:
        items[0][title]: Lecture sessions
        items[0][attendance]: 1
        items[0][weight]: 0.3
        items[0][total]: 33
        items[1][title]: Lab sessions
        items[1][attendance]: 2
        items[1][weight]: 0.4
        items[1][total]: 22
        items[2][title]: Support sessions
        items[2][attendance]: 3
        items[2][weight]: 0.15
        items[2][total]: 44
        items[3][title]: Canvas activities
        items[3][attendance]: 4
        items[3][weight]: 0.15
        items[3][total]: 55

    Response Headers:
        Content-Type: application/json
    Response Body:
        {
            "sum": 10
        }
    ```

### Description of Testing:

Two integration tests for successful and failure case.

Command:
```sh
pytest -v
```

### Anything else to highlight for Function Three:

To start development run these commands first:
```sh
python3 -m venv .venv
. .venv/bin/activate
pip install -r requirements.txt
```

## Function Four

### Function (What Operation):

Student Engagement Score (score)

### Repository URL:

[/backend/score](/backend/score/)

### Live Service URL:

http://score.jmproject.site

### Description of Implementation (language, methods, paradigm, etc):

- Language - Go
- Framework - not needed
- API:
    ```txt
    POST /
    Request Headers:
        Content-Type: application/x-www-form-urlencoded
    Request Body:
        items[0][title]: Lecture sessions
        items[0][attendance]: 1
        items[0][weight]: 0.3
        items[0][total]: 33
        items[1][title]: Lab sessions
        items[1][attendance]: 2
        items[1][weight]: 0.4
        items[1][total]: 22
        items[2][title]: Support sessions
        items[2][attendance]: 3
        items[2][weight]: 0.15
        items[2][total]: 44
        items[3][title]: Canvas activities
        items[3][attendance]: 4
        items[3][weight]: 0.15
        items[3][total]: 55

    Response Headers:
        Content-Type: application/json
    Response Body:
        {
            "score": 0.1
        }
    ```

### Description of Testing:

Two integration tests for successful and failure case.

Command:
```sh
go test ./...
```

### Anything else to highlight for Function Four:

To start development run this command first:
```sh
go mod download
```

## Function Five

### Function (What Operation):

Risk of Student Failure (risk)

### Repository URL:

[/backend/risk](/backend/risk/)

### Live Service URL:

http://risk.jmproject.site

### Description of Implementation (language, methods, paradigm, etc):

- Language - nodejs
- Framework - express
- API:
    ```txt
    POST /
    Request Headers:
        Content-Type: application/x-www-form-urlencoded
    Request Body:
        score: 0.2
        cutoff: 0.3
    Response Headers:
        Content-Type: application/json
    Response Body:
        {
            "risk": true
        }
    ```

### Description of Testing:

Two integration tests for successful and failure case.

Command:
```sh
npm test
```

### Anything else to highlight for Function Five:

To start development run this command first:
```
npm install
```

## Function Six

### Function (What Operation):

Student Recognition (recognition)

### Repository URL:

[/backend/recognition](/backend/recognition/)

### Live Service URL:

http://recognition.jmproject.site

### Description of Implementation (language, methods, paradigm, etc):

- Language - Java
- Framework - Spring Boot
- API:
    ```txt
    POST /
    Request Headers:
        Content-Type: application/x-www-form-urlencoded
    Request Body:
        score: 0.2
    Response Headers:
        Content-Type: application/json
    Response Body:
        {
            "recognized": false
        }
    ```

### Description of Testing:

Two integration tests for successful and failure case.

Command:
```sh
./mvnw clean test
```

### Anything else to highlight for Function Six:

To start development run this command first to download dependencies:
```sh
./mvnw dependency:resolve
```


## Improvements

### Detail here what you improved, why, and how. 

To demonstrate that these improvements are made and working include screenshots, code, or any descriptions of what you’ve done.

1. Refactored initial two PHP functions to use Slim framework and PHPUnit for easier testing and error handling.
2. Standardized request data structures so it could be easier to keep similar validation across different microservices. Also it makes easier prepare requests from frontend.
3. Added configurable URL support to frontend. They can be configured via environment variables:
    ```txt
    MAXMIN_URL=http://localhost:8081/
    SORT_URL=http://localhost:8082/
    SUM_URL=http://localhost:8083/
    SCORE_URL=http://localhost:8084/
    RISK_URL=http://localhost:8085/
    PROXY_URL=http://localhost:8888/
    PROXY_ENABLED=false
    ```
4. Dockerized all components
5. Added Gitlab CI to test and build all components.


### Anything else to highlight:


## Custom Proxy

### Repository URL:

[/backend/proxy](/backend/proxy/)

### Live Service URL:

http://proxy.jmproject.site

### Implementation Details (how you implemented, what it does, how it works, etc):

- Language - Go
- Framework - not needed
- Configuration - using yaml file `/app/config.yml`
    ```yml
    services:
    - path: /maxmin
      addr: http://backend-maxmin:80
    - path: /risk
      addr: http://backend-risk:80
    - path: /score
      addr: http://backend-score:80
    - path: /sort
      addr: http://backend-sort:80
    - path: /sum
      addr: http://backend-sum:80

    ```

### Testing Details (how you tested):

Written integration tests to ensure proxy is working. Also this can be tested manually by starting all services using `docker-compose` and enabling `PROXY_ENABLED=true` setting on `frontend` component.

### Brief Review of Success (did it work):

Yes

### Anything else to highlight:


## Frontend Failure Handler

### Implementation Details (how you implemented, what it does, how it works, etc):

Switched to `fetch` with `await/async` flow. This simplifies error handling. Example of one method:

```js
async function getMaxMin() {
    try {
        const res = await fetch(maxminURL, {
            body: getFormData(),
            method: "post",
            headers: {
              'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
            },
        });
        const data = await res.json();
        if (data.error) {
            throw new Error(data.error);
        }
        displayMaxMin(data.max, data.min);
    } catch (err) {
        console.error(err);
        alert(err.message);
    }
}
```

This simple implementation creates alert in case there is a custom domain error or even a network issue. But it does require all microservices to respond with similar `{ "error": string }` response.

### Testing Details (how you tested):

Changed form inputs to cause validation error or stopped services to cause connectivity issues. Frontend tested manually.

### Brief Review of Success (did it work):

Yes

### Anything else to highlight:

